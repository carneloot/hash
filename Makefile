

SRCS = tests.c hash.c
OBJS = $(SRCS:%.c=%.o)

hash: $(OBJS)
	gcc -Wall -lm -g -o $@.out $(OBJS)

tests.o: tests.c 
	gcc -c -Wall -lm -g -o $@ $<

%.o: %.c %.h
	gcc -c -Wall -lm -g -o $@ $<

.PHONY: remove
remove:
	rm -rf $(OBJS) kdtree